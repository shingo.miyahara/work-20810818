<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%
	ArrayList<String> items = (ArrayList<String>) request.getAttribute("items");
	if (items == null) {
		response.sendRedirect("./init");
	}
%>
<!DOCTYPE>
<html>
<body>
	<form action="add" method="post">
		<div class="text">
			<input type="text" name="text">
		</div>
		<div class="button">
			<button type="submit">Submit!!</button>
		</div>
	</form>
	<%
		if (items != null) {
			for (String item : items) {
	%>
	<table>
		<tr>
			<td><%=item%></td>
		</tr>
	</table>
	<%
		}
	}
	%>
	<form action="delete" method="post">
		<div class="delete">
			<button type="submit" name="button" value="delete">Clear</button>
		</div>
	</form>
</body>
</html>
