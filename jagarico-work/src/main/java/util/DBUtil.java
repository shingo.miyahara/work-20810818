package util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBUtil {

	public Connection con;
	private static final String HOST = "jdbc:postgresql://35.200.68.176:5432/db";
	private static final String USER = "postgres";
	private static final String PASS = "needswell";
	public DBUtil() {
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(HOST, USER, PASS);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public List<String> select() throws SQLException {
		List<String> items = new ArrayList<String>();
		PreparedStatement stm = this.con.prepareStatement("select item from items");
		ResultSet rs = stm.executeQuery();
		while(rs.next()){
			items.add(rs.getString("item"));
		}
		return items;
	}
	public void insert(String item) throws SQLException {
		PreparedStatement stm = this.con.prepareStatement("insert into items values(?)");
		stm.setString(1, item);
		stm.executeUpdate();
	}
	public void delete() throws SQLException {
		PreparedStatement stm = this.con.prepareStatement("delete from items");
		stm.executeUpdate();
	}

	public void Close() {
		if (this.con != null) {
			try {
				this.con.close();
			} catch (Exception e) {
				System.out.println("MySQLのクローズに失敗しました。");
			}
		}
	}
}