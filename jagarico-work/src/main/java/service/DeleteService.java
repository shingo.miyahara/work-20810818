package service;

import java.sql.SQLException;

public class DeleteService extends AbstractService{

	public  void delete() {
		try {
			super.db.delete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
