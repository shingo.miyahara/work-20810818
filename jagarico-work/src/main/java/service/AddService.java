package service;

import java.sql.SQLException;

public class AddService extends AbstractService{

	public void add(String text) {
		try {
			super.db.insert(text);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
