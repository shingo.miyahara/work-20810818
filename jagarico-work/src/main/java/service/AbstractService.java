package service;

import java.sql.SQLException;
import java.util.List;

import util.DBUtil;

public class AbstractService {

	protected DBUtil db;
	public AbstractService() {
		this.db = new DBUtil();
	}
	public List<String> select() {
		List<String> items = null;
		try {
			items = this.db.select();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items;
	}
	public void close(){
		this.db.Close();
	}
}
