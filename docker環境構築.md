# docker環境構築

実行環境 CentOS7
公式手順
https://docs.docker.com/install/linux/docker-ce/centos/

1. 古いバージョンのアンインストール(今回は不要)
```
sudo yum remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-selinux \
                docker-engine-selinux \
                docker-engine
```
2. 必要パッケージのインストール
```
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```
3. レポジトリの追加
```
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```
4. dockerのインストール
```
sudo yum install docker-ce
```
5. dockerの起動
```
sudo systemctl start docker
```
6. dockerの実行
```
sudo docker run hello-world
```
7. 通常ユーザーで実行できないことを確認
```
docker run hello-world
```
8. 通常ユーザーをdockerグループに
```
sudo usermod -G docker $USER
```
9. dockerの再起動
```
sudo systemctl restart docker
```
10. 再度、通常ユーザーでdockerの実行
```
docker run hello-world
```

## docker-composeインストール
https://docs.docker.com/compose/install/
```
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
バイナリの実行権限の変更
```
sudo chmod +x /usr/local/bin/docker-compose
```
