# dcoker-composeを使ってみる

サンプル
```
version: '3'
services:
  httpd:
    container_name: httpd
    image: httpd
    ports:
      - "80:80"
```
docker-compose up -d
-d: バックグラウンド実行

```
version: '3'
services:
  app:
    container_name: app
    build: ./java
    image: app
    command: java Sample Jagarico
```
docker-compose up --build
--build: ビルドして実行