# dockerを使ってみる

## Webサーバー(Apache)の環境構築
```
docker run --name web-server -d -p 80:80 httpd
```
外部IPをブラウザに入力
終了

```
docker ps
CONTAINER ID        IMAGE                                       COMMAND                  CREATED             STATUS              PORTS                    NAMES
d3228e32cf5d        httpd                                       "httpd-foreground"       6 minutes ago       Up 6 minutes        0.0.0.0:80->80/tcp       web-server
```
```
docker exec -it d3228e32cf5d bash
cat ./htdocs/index.html
```

## ItWorksの編集
```
mkdir -p ./httpd/src
cd ./httpd/src
vi index.html
<html><body><h1>Jagarico works!</h1></body></html>
docker stop d3228e32cf5d
docker rm d3228e32cf5d
docker run --name web-server -d -p 80:80 -v /home/shlngo1019_miya/httpd/src:/usr/local/apache2/htdocs httpd
```

他にもある、docker公式イメージを使って遊ぼう!!
- MySQL
- WordPress
- Jenkins
- Redmineなどなど